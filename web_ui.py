
import time
import paramiko
import json
from random import randint
from multiprocessing.pool import ThreadPool


def get_hosts(host_file):
    f = open(host_file, "r")
    contents = f.read()
    hosts = contents.replace("\n", " ")
    ip_lst = hosts.split()
    return ip_lst


def get_cpu_info(host):
    count = 0
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh.load_system_host_keys()
    try:
        ssh.connect(host, username="pi", password="raspberry", timeout=0.5)
        stdin, stdout, stderr = ssh.exec_command("top -b -n 10 -d.2 | grep 'Cpu' |  awk 'NR==3{ print($2)}'")
        host_cpu_usage = stdout.readlines()[0].strip('\n')
        stdin.close()
    except:
        host_cpu_usage = '0'
        pass
    finally:
        count += 1
        ssh.close()
    return host_cpu_usage


def parallel_ssh_connect(hosts):
    cpu_data = dict()
    pool = ThreadPool(28)
    results = []
    for host in hosts:
        results.append(pool.apply_async(get_cpu_info, args=(host,)))
    pool.close()
    pool.join()
    results = [r.get() for r in results]
    for i in range(len(results)):
        cpu_data.update({hosts[i]: results[i]})
    return cpu_data


def dummy_data():
    data = dict()
    for i in range(16):
        num = randint(50, 100)
        data.update({i: num})
    return data


def query_for_cpu():
    hosts = get_hosts("nodes.txt")
    while True:
        data = parallel_ssh_connect(hosts)
        # data = dummy_data()
        # print(data)
        with open("/var/www/html/node_info.json", 'w') as file:
            json.dump(data, file)
        time.sleep(2)


def main():
    query_for_cpu()
    

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass


